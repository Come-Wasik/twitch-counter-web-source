# Counter

This is my personal counter (web source display) for Streaming (like Twitch).

## Usage

Clone this project, and open public/index.html

The url can be updated in order to change the timer or the "after message".

?seconds=integer<0-60>
?minutes=integer<0-60>
?text-end=string


## Compile assets (Js and Css)

### For production
docker-compose run node

### For developement
docker-compose run node npm run development
docker-compose run node npm run watch

## Manually transpile css and JS
npx mix

## Manually transpile tailwindcss
npx tailwindcss -i ./src/app.css -o ./public/app.css --watch

# Licence

[MIT](./LICENCE)
