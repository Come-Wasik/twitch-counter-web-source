let mix = require('laravel-mix');

mix.js('src/app.js', 'js')
    .setPublicPath('public')
    .postCss("src/app.css", "css", [
        require("tailwindcss"),
    ]);
