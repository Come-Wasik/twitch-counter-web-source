import Alpine from 'alpinejs'
import mycounter from './mycounter.js'

Alpine.data('mycounter', mycounter)

window.Alpine = Alpine;
Alpine.start();
