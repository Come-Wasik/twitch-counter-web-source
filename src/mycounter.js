export default () => ({
    count: {
        seconds: 3,
        minutes: 0,
    },
    displayTimer: true,
    textEnd: "Fin",
    intervalId: 0,

    init() {
        const urlParams = new URLSearchParams(window.location.search);
        this.count.seconds = Number(urlParams.get('seconds') ?? this.count.seconds);
        this.count.minutes = Number(urlParams.get('minutes') ?? this.count.minutes);
        this.textEnd = urlParams.get('text-end') ?? this.textEnd;

        this.intervalId = setInterval(() => this.decrementTime(this), 1000)
    },

    decrementTime(self) {
        // Timer is finished
        if (self.count.minutes === 0 && self.count.seconds === 1) {
            this.displayTimer = false;
            clearInterval(self.intervalId);
        }

        // A minute has passed
        if (self.count.minutes > 0 && self.count.seconds === 0) {
            self.count.seconds = 59;
            self.count.minutes -= 1;
        } else {
            // A second has passed
            self.count.seconds -= 1;
        }
    },

    /**
     *
     * @param {Number} number The number to pad as timer unit
     * @returns {String}
     */
    padTimerLeft(number) {
        return number.toString().padStart(2, '0')
    }
});
