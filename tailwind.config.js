/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./public/**/*.html",
        "./src/**/*.{css,js}",
    ],
    theme: {
        extend: {
            colors: {
                'primary': '#07e1ab'
            },
            fontFamily: {
                'coffee': ['Coffee Spark', 'sans-serif'],
            }
        },
    },
    plugins: [],
}
